

## Requisitos
Un paquete de apache con MYSQL Y PHP en su ultima versión.
Conexión a internet(utliza los servidores de pusher para el control del broadcast)

## Clonar un repositorio
---
Utilice estos pasos para clonar desde SourceTree, nuestro cliente para usar la línea de comandos del repositorio de forma gratuita. La clonación le permite trabajar en sus archivos localmente. Si aún no tiene SourceTree, [descargue e instale primero] (https://www.sourcetreeapp.com/). Si prefiere clonar desde la línea de comandos, consulte [Clonar un repositorio](https://confluence.atlassian.com/x/4whODQ).

1. Verá el botón de clonación debajo del encabezado **Fuente**. Haga clic en ese botón.
2. Ahora haga clic en **Consultar en SourceTree**. Es posible que deba crear una cuenta de SourceTree o iniciar sesión.
3. Cuando vea el cuadro de diálogo **Clonar nuevo** en SourceTree, actualice la ruta de destino y el nombre si lo desea y luego haga clic en **Clonar**.
4. Abra el directorio que acaba de crear para ver los archivos de su repositorio.
---
## Ejecución
1. despues de Clonar en repositoria es nesesario ejecutar y crear un vhost con el dominio local.pruebas.com o en su defecto ejecutar el comando php artisan serve 
2. Creamos la tabla en la base de datos con el nombre tic_laravel.
3. Modifcamos el archivo .env y configuramos la conexión a la base de datos con los datos de su conexión por defecto viene con:
DB_HOST=127.0.0.1
DB_PORT=3306
4. ejecutamos las migraciones a la base de datos con el comando:
php artisan migrate
5. ejecutamos el servidor apache O con el comando php artisan serve
6. Ingremos al dominio local.pruebas.com o a el servidor que se ejecute con el comando php artisan serve
6. Todo listo para jugar TIC-TAC-TOE :)


