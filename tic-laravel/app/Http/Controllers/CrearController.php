<?php
/**
 * @author David herrera
 * @version 1.0v
 * 
 * Considero mejorar el sistema optimizando las respuestas del back-end
 * finalizar el sistema invitado espectador 
 * crear un usuario registrado para a la hora de ingresar al juego 
 * optimizar la verificación del estado del juego.
 * 
 *                      puntos a tener encuenta
 * 
 * No me involucre a revisar falencia seguridad ni bugs por el tiempo de desarrollo
 * Desconosco los estandares de calidad de la empresa por ende si me equvoco en algo espero un comentario al respecto
 * No manejo laravel front muy seguido pero tengo el conocmiento basico en platillas blade 
 */
namespace App\Http\Controllers;

use App\Models\Partida;
use Illuminate\Http\Request;

// controlador encargado de manejar el inicio del juego 
class CrearController extends Controller
{
    public function index(){
        return view("Crear");
    }
    public function CrearPartida(Request $request){
        $puestos = []; 
        for($i = 0; $i < 9; $i++){
            $datos = array("id"=>$i,
                "estado"=>false,
                "tipo"=>0);
        array_push($puestos,$datos);
    
        };
        $partida = new Partida;
        $partida->c1 = $request->Nombre;
        $partida->c2 = 0;
        $partida->turno = 1;
        $partida->stats_1 = json_encode([]);
        $partida->stats_2 = json_encode([]);
        $partida->key = random_int(1000, 10000);
        $partida->estado = 1;
        $partida->campos = json_encode($puestos);
        $partida->save();
        return redirect("juego/".$partida->key."/".$request->Nombre);
    }
}
