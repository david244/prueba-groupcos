<?php
/**
 * @author David herrera
 * @version 1.0v
 * 
 * Considero mejorar el sistema optimizando las respuestas del back-end
 * finalizar el sistema invitado espectador 
 * crear un usuario registrado para a la hora de ingresar al juego 
 * optimizar la verificación del estado del juego.
 * 
 *                      puntos a tener encuenta
 * 
 * No me involucre a revisar falencia seguridad ni bugs por el tiempo de desarrollo
 * Desconosco los estandares de calidad de la empresa por ende si me equvoco en algo espero un comentario al respecto
 * No manejo laravel front muy seguido pero tengo el conocmiento basico en platillas blade 
 */
namespace App\Http\Controllers;

use App\Events\GanadorEvento;
use App\Events\iniciarEvento;
use App\Events\juegoEvento;
use App\Events\ReiniciarEvento;
use App\Models\Partida;
use Illuminate\Http\Request;


// Todo lo realizado al juego y flujo en vivo lo maneja este controllador 

class juegoController extends Controller
{
    public function index(Request $request){
         $partida = Partida::where('key',$request->key)->first();
         $jugador = "";
         $campos = json_decode($partida->campos);     
        if($partida->c1 == $request->user){
            $jugador = 1;
            event(new juegoEvento($jugador,1,$request->key,1,$partida->estado));
        }else if($partida->c2 == $request->user){
            $jugador = 2;
            event(new juegoEvento($jugador,1,$request->key,1,$partida->estado));
        }else{
            $jugador = 3;
            event(new juegoEvento($jugador,3,$request->key,1,$partida->estado));
        }
        return view("juego",compact('partida','jugador','campos'));
    }

    public function Click(Request $request){
        $partida = Partida::where('key',$request->key)->first();
        if($request->user == 1){
            $partida->turno = 2;
            $partida->stats_1 = json_encode($request->juego);
        }else if($request->user == 2){
            $partida->turno = 1;
            $partida->stats_2 = json_encode($request->juego);
        }
        $partida->save();
        $clicsk = json_encode($request->juego);

        event(new iniciarEvento($request->click,$partida->turno));
        return response($partida."click".$request->click,200);
    }
    public function Ganar(Request $request){
        $partida = Partida::where('key',$request->key)->first();
        $partida->estado = 3;
        event(new GanadorEvento($request->jugador));
        return response("user ".$request->jugador,200);
    }
    public function Reinicio(Request $request){
        $partida = Partida::where('key',$request->key)->first();
        $partida->estado = 1;
        event(new ReiniciarEvento($request->key));
        return response($request->key,200);
    }
}
