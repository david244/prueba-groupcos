<?php
/**
 * @author David herrera
 * @version 1.0v
 * 
 * Considero mejorar el sistema optimizando las respuestas del back-end
 * finalizar el sistema invitado espectador 
 * crear un usuario registrado para a la hora de ingresar al juego 
 * optimizar la verificación del estado del juego.
 * 
 *                      puntos a tener encuenta
 * 
 * No me involucre a revisar falencia seguridad ni bugs por el tiempo de desarrollo
 * Desconosco los estandares de calidad de la empresa por ende si me equvoco en algo espero un comentario al respecto
 * No manejo laravel front muy seguido pero tengo el conocmiento basico en platillas blade 
 */
namespace App\Http\Controllers;

use App\Models\Partida;
use Illuminate\Http\Request;

// maneja el ingreso del usuario 2 desde el formulario front-end

class UnirseController extends Controller
{
    public function index(){
        return view("unirse");
    }
    public function UnirsePartida(Request $request){
        $partida = Partida::where('key',$request->key)->first();
        $partida->c2 = $request->Nombre;
        $partida->turno = 1;
        $partida->stats_1 = json_encode([]);
        $partida->stats_2 = json_encode([]);
        $partida->estado = 2;
        $partida->save();
        return redirect("juego/".$partida->key."/".$request->Nombre);
    }
}
