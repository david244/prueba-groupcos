<?php
/**
 * @author David herrera
 * @version 1.0v
 * 
 * Considero mejorar el sistema optimizando las respuestas del back-end
 * finalizar el sistema invitado espectador 
 * crear un usuario registrado para a la hora de ingresar al juego 
 * optimizar la verificación del estado del juego.
 * 
 *                      puntos a tener encuenta
 * 
 * No me involucre a revisar falencia seguridad ni bugs por el tiempo de desarrollo
 * Desconosco los estandares de calidad de la empresa por ende si me equvoco en algo espero un comentario al respecto
 * No manejo laravel front muy seguido pero tengo el conocmiento basico en platillas blade 
 */

namespace App\Events;

use Illuminate\Broadcasting\Channel;
use Illuminate\Broadcasting\InteractsWithSockets;
use Illuminate\Contracts\Broadcasting\ShouldBroadcastNow;
use Illuminate\Foundation\Events\Dispatchable;
use Illuminate\Queue\SerializesModels;

// evento que controla el inicio del juego 
class iniciarEvento implements ShouldBroadcastNow
{
    use Dispatchable, InteractsWithSockets, SerializesModels;

    public $click;
    public $turno;
    public function __construct($click,$turno)
    {
        $this->click = $click;
        $this->turno = $turno;
    }
    public function broadcastOn()
    {
        return new Channel('Juego-inicia');
    }
    public function broadcastAs()
    {
        return "inicio";
    }
}
