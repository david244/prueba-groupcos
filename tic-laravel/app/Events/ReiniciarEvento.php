<?php
/**
 * @author David herrera
 * @version 1.0v
 * 
 * Considero mejorar el sistema optimizando las respuestas del back-end
 * finalizar el sistema invitado espectador 
 * crear un usuario registrado para a la hora de ingresar al juego 
 * optimizar la verificación del estado del juego.
 * 
 *                      puntos a tener encuenta
 * 
 * No me involucre a revisar falencia seguridad ni bugs por el tiempo de desarrollo
 * Desconosco los estandares de calidad de la empresa por ende si me equvoco en algo espero un comentario al respecto
 * No manejo laravel front muy seguido pero tengo el conocmiento basico en platillas blade 
 */
namespace App\Events;

use Illuminate\Broadcasting\Channel;
use Illuminate\Broadcasting\InteractsWithSockets;
use Illuminate\Broadcasting\PresenceChannel;
use Illuminate\Broadcasting\PrivateChannel;
use Illuminate\Contracts\Broadcasting\ShouldBroadcast;
use Illuminate\Foundation\Events\Dispatchable;
use Illuminate\Queue\SerializesModels;

// evento que permite notificar el reinicio del juego
class ReiniciarEvento implements ShouldBroadcast
{
    use Dispatchable, InteractsWithSockets, SerializesModels;

    public $key;
    public function __construct($key)
    {
        $this->key = $key;
    }

    /**
     * Get the channels the event should broadcast on.
     *
     * @return \Illuminate\Broadcasting\Channel|array
     */
    public function broadcastOn()
    {
        return new Channel('reinicio-channel');
    }
    public function broadcastAs()
    {
        return "reinicio";
    }
}
