<?php
/**
 * @author David herrera
 * @version 1.0v
 * 
 * Considero mejorar el sistema optimizando las respuestas del back-end
 * finalizar el sistema invitado espectador 
 * crear un usuario registrado para a la hora de ingresar al juego 
 * optimizar la verificación del estado del juego.
 * 
 *                      puntos a tener encuenta
 * 
 * No me involucre a revisar falencia seguridad ni bugs por el tiempo de desarrollo
 * Desconosco los estandares de calidad de la empresa por ende si me equvoco en algo espero un comentario al respecto
 * No manejo laravel front muy seguido pero tengo el conocmiento basico en platillas blade 
 */
namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

// modelo del la partida para consultas 
class Partida extends Model
{
    use HasFactory;

    protected $table = 'partidas';
    protected $fillable = ['c1','c2','turno','estado','stats_1','stats_2','key','estado','campos'];
}
