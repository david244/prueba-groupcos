{{-- vista --}}
@extends('layouts.inicio')
@section('Index')
<div class="conteiner-sm-fluid">

    <div class="row text-center">
        <h1>turno de jugador</h1>
        <h1>partida N°:{{$partida->key}}</h1>
        <h1 style="display: none" id="users">{{$jugador}}</h1>
        <h1 style="display: none" id="key">{{$partida->key}}</h1>
        <h1 style="display: none" id="turnos">{{$partida->turno}}</h1>
        <h1 id="turno">turno de jugador: {{$partida->turno}}</h1>
       <div class="col-4">
        <div class="box-user shadow p-3 mb-5 bg-body rounded box-form">
            <h1>Jugador 1</h1>
            @if ($jugador == 1)
            <h1>Yo</h1>
            @endif
            <h1 id="j1">{{$partida->c1}}</h1>
        </div>
       </div>
       <div class="col-4">
           <div class="shadow p-3 mb-5 bg-body rounded box-form text-center">
               <div id = "tablero" class="box text-center">
                    @foreach ($campos as $item)
                        <div id = "{{$item->id}}" class="box-jr" onclick="clicks({{$item->id}});">
                        @if($item->tipo <= 0||$item->estado === false)
                            <h1 id = "h{{$item->id}}">-</h1>                       
                        @else
                            @if ($item['tipo'] == 1)
                                <h1 id = "h{{$item->id}}">X</h1>  
                            @else
                                @if (($item['tipo'] == 2))
                                    <h1 id = "h{{$item->id}}">O</h1>
                                @endif
                            @endif
                        @endif

                        </div>
                    @endforeach  
                    </form>
               </div>
           </div>
           <div id="mensajes">

           </div>
       </div>
       <div class="col-4">
        <div class="box-user shadow p-3 mb-5 bg-body rounded box-form">
            <h1>jugador 2</h1>
            @if ($jugador == 2)
                <h1>Yo</h1>
            @endif
            <h1 id="j2">@if ($partida->c2 == 0)
                esperando...
                @else
                {{$partida->c2}}
            @endif</h1>
        </div>
       </div>
        
    </div>
    </div>
</div>
@endsection