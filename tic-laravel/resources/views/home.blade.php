{{-- vista --}}
@extends('layouts.inicio')

@section('Index')
<div class="conteiner-sm-fluid ">
    <div class="row text-center">
        <div class="row"><h3 class="top-50">TIC-TAC-TOE</h3></div>
        <div class="col text-center position-absolute top-50 start-50 translate-middle">
           <div class="row">
               <div class="col-6">
                    <div class="card">
                        <div class="card-body">
                          <h5 class="card-title">Nuevo juego</h5>
                          <p class="card-text">Crea un nuevo juego.</p>
                          <a href="iniciar" class="btn btn-primary">iniciar</a>
                        </div>
                    </div>
               </div>
               <div class="col-6">
                <div class="card">
                    <div class="card-body">
                      <h5 class="card-title">Unirse</h5>
                      <p class="card-text">Unete a un juego ya existente</p>
                      <a href="unirse" class="btn btn-primary">Unirse</a>
                    </div>
                </div>
           </div>
           </div>
        </div>
    </div>
</div>
@endsection