{{-- vista --}}
@extends('layouts.inicio')

@section('Index')
<div class="conteiner-sm-fluid">
    <div class="row text-center">
       <div class="col"></div>
       <div class="col">
        <form class="shadow p-3 mb-5 bg-body rounded box-form" method="POST">
          @csrf
            <div class="mb-3">
              <label class="form-label">Nombre</label>
              <input type="text" class="form-control" id="Nombre" name="Nombre">
              <div id="emailHelp" class="form-text"></div>
            </div>
            <div class="mb-3">
              <label for="exampleInputPassword1" class="form-label">Sala id:</label>
              <input type="text" class="form-control" id="exampleInputPassword1" name="key">
              <div id="emailHelp" class="form-text">Ingresa el codigo de la sala de tu amigo</div>
            </div>
            <button type="submit" class="btn btn-primary">entrar</button>
          </form>
       </div>
       <div class="col"></div>

    </div>
</div>
@endsection