<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\CrearController;
use App\Http\Controllers\juegoController;
use App\Http\Controllers\UnirseController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('home');
});
Route::get('/iniciar',[CrearController::class, 'index']);
Route::get('/unirse',[UnirseController::class, 'index']);
Route::get('/juego/{key}/{user}',[juegoController::class, 'index']);
Route::post('/iniciar',[CrearController::class, 'CrearPartida']); 
Route::post('/unirse',[UnirseController::class, 'UnirsePartida']);
