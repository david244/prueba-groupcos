<?php
/**
 * @author David herrera
 * @version 1.0v
 * 
 * Considero mejorar el sistema optimizando las respuestas del back-end
 * finalizar el sistema invitado espectador 
 * crear un usuario registrado para a la hora de ingresar al juego 
 * optimizar la verificación del estado del juego.
 * 
 *                      puntos a tener encuenta
 * 
 * No me involucre a revisar falencia seguridad ni bugs por el tiempo de desarrollo
 * Desconosco los estandares de calidad de la empresa por ende si me equvoco en algo espero un comentario al respecto
 * No manejo laravel front muy seguido pero tengo el conocmiento basico en platillas blade 
 */
use App\Http\Controllers\juegoController;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

// Rutas de consulta 
Route::post("estado/{key}",[juegoController::class,'Click'])->name("estado");
Route::post('ganar/{key}',[juegoController::class,'Ganar'])->name("ganar");
Route::post('reinicio/{key}',[juegoController::class,'Reinicio'])->name("reinicio");