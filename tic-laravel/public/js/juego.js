
/**
 * @author David herrera
 * @version 1.0v
 * 
 * Considero mejorar el sistema optimizando las respuestas del back-end
 * finalizar el sistema invitado espectador 
 * crear un usuario registrado para a la hora de ingresar al juego 
 * optimizar la verificación del estado del juego.
 * 
 *                      puntos a tener encuenta
 * 
 * No me involucre a revisar falencia seguridad ni bugs por el tiempo de desarrollo
 * Desconosco los estandares de calidad de la empresa por ende si me equvoco en algo espero un comentario al respecto
 * No manejo laravel front muy seguido pero tengo el conocmiento basico en platillas blade 
 */
/** Varaiables de entorno */
const ganador = false;
var turno = 1;
const users = 0;
const o = [];
const x = [];
var tablero = []
var key = ""
var casilla = "";
key = document.getElementById("key").innerHTML;
turno = parseInt(document.getElementById("turnos").innerHTML);
//** instancia de pusher */
var pusher = new Pusher('d0d1c484c6aec6aebb8f', {
    cluster: 'us2'
    });
    /**Canales broadcast */
    var channel_inicio = pusher.subscribe('Juego-inicia');
    channel_inicio.bind('inicio',function(data){
        console.log(data)
        turno = data.turno;
        var enviar = "";
        elemento = document.getElementById("turno").innerHTML = 'turno de jugador:'+turno;
        if(data.turno == 2){
            enviar = "X"
        }else{
            enviar = "O"
        }
        turnos(data.click,enviar);
        
    })
    var channel = pusher.subscribe('estado-juego');
    channel.bind('Entrada-user', function(data) {
        console.log(data)
        if(data.estadoj == 1){
            elemento = document.getElementById("tablero").innerHTML = `                    <br>
            <div class="text-center">
                <h2 class=>Esperando jugador para comenzar..</h2>
                <h3>Codigo de la sala:</h3>
                <div id ="key" onclick="copiarAlPortapapeles('key')" class="alert alert-success" role="alert">
                ` +data.key+` 
                  </div>
            </div>`
        }else if (data.estadoj == 2){
            elemento = document.getElementById("mensajes").innerHTML = `           
             <div class="alert alert-success" role="alert">
            Jugador se a conectado `+data.usuario+`
            </div>`

        }
    
    });
    var channel_Ganador = pusher.subscribe('Ganador');
    channel_Ganador.bind('Gano', function(data) {
        console.log(data)
        document.getElementById('tablero').innerHTML = "<br><h1>gano: "+data.ganador+"</h1>";
        elemento = document.getElementById("mensajes").innerHTML = `    
        <button type="button" class="btn btn-primary" onclick="reiniciar(`+key+`);">Reiniciar</button>`
    });
    var channel_Reinicio = pusher.subscribe('reinicio-channel');
    channel_Reinicio.bind('reinicio', function(data) {
        console.log(data)
        if(data.key == key){
            location.reload();
        }
    });
       /**FIN Canales broadcast */

/**Funciones
 * @copiarAlPortapapeles
 * Copea el texto del componente seleccionado.
 * @clicks
 * ejecuta la acción del tablero, con  todas sus funcionalidades.
 *  * @reiniciar
 * Reinicia el juego llamando al evento con jquery. 
 *  * @eventox
 * ejecuta el evento de estado de juego y notifica al sistema el click realizado.
 *  * @eventog
 * Notifica el ganador el juego.
 *  * @verificar
 * verifica el ganador del juego.
 *  * @CasillaController
 * Controla los aspectos basicos de cada casilla .
 *  * @turnos
 * controla las casillas por el turno actual.
 */
function copiarAlPortapapeles(id_elemento) {
    var aux = document.createElement("input");
    aux.setAttribute("value", document.getElementById(id_elemento).innerHTML);
    document.body.appendChild(aux);
    aux.select();
    document.execCommand("copy");
    document.body.removeChild(aux);
    alert("Codigo copeado!!");
    }
function clicks(item){
    user = document.getElementById("users").innerHTML;
    if(turno == 1 &&  parseInt(user) == 1){
        casilla =CasillaController("h"+item,'X')
        if(casilla == 0){
            x.push(item)
            turno++
            verificar(x,"j1");
            eventox('h'+item,1,x)
        }
    }else if(turno == 2  &&  parseInt(user) == 2){
        casilla = CasillaController("h"+item,'O')
        if(casilla == 0){
            o.push(item)
            turno--
            verificar(o,"j2");
            eventox("h"+item,2,o)
        }
    }else{
        alert("Es el turno del jugador: "+turno)
    }

   }
   function reiniciar(key){
        console.log(key)
        $.post('http://local.pruebas.com/api/reinicio/'+key, 
        {  "_token": $("meta[name='csrf-token']").attr("content"), }
        , function(resp) {
            console.log(resp);
        });
   }
   function eventox(h1,user,estado){
    $.post('http://local.pruebas.com/api/estado/'+key, 
    { click :h1,user :user,juego:estado, "_token": $("meta[name='csrf-token']").attr("content"), }
    , function(resp) {
        console.log(resp);
    });
   }
   function eventog(ganador){
       console.log(ganador+"console de eventog")
    $.post('http://local.pruebas.com/api/ganar/'+key, 
    { jugador :ganador, "_token": $("meta[name='csrf-token']").attr("content"), }
    , function(resp) {
        console.log(resp);
    });
   }
   function verificar(xs,jugador){
    const winningConditions = [
        [0, 1, 2],
        [3, 4, 5],
        [6, 7, 8],
        [0, 3, 6],
        [1, 4, 7],
        [2, 5, 8],
        [0, 4, 8],
        [2, 4, 6]
    ];
            for(i = 0; i < 8; i++){
                var gana = 0
                var ganar = winningConditions[i]
                var a = ganar[0]
                var b = ganar[1]
                var c = ganar[2]
                xs.forEach(element =>{
                    if(element == a || element == b ||element == c ){
                        gana++
                        if(gana == 3){
                            obj = document.getElementById(jugador).innerHTML;
                            eventog(obj);
                        }
                    }
                })
            } 
        }  
   function CasillaController(element,elemento){
        estado = 0; 
        console.log(element)
        console.log(String(element))
        obj = document.getElementById(element);
        console.log(obj.innerHTML)
        if(obj.innerHTML == '-'){
            turnos(element,elemento);
        }else{
            alert("casilla ya utilizada")
            estado = 1;
        }
        return estado;
   }
   function turnos(h1,cambio){

        elemento = document.getElementById(h1).innerHTML = cambio ;
     return elemento;
   }


